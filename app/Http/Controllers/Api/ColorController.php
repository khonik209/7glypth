<?php

namespace App\Http\Controllers\Api;

use App\Color;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ColorController extends Controller {

	/**
	 * Return all colors
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function getList() {
		$colors = Color::orderBy('name','asc')->get();
		return response()->json( [
			$colors
		], 200 );
	}
}
