<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

class ApiController extends Controller
{
    /**
     * Первый шаг oAuth авторизации. Выдача кода авторизации
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function authConnect(Request $request)
    {
        $this->validate($request, [
            'client_id' => env('APP_ENV') == "local" ? 'integer' : 'required|integer',
            'redirect_uri' => env('APP_ENV') == "local" ? 'string' : 'required|string',
            'client_secret' => env('APP_ENV') == "local" ? 'string' : 'required|string',
        ]);

        /* todo: заменить на актуальные данные пользователя при тестировании. В production - получаем из Request. */
        $testUser = [
            'id' => '3',
            'uri' => 'http://test.local/callback',
            'secret' => 'NeR8GDzUsnb4zFCJjHP28Hpwxl5aTAIleBjy2zZR'
        ];
        $query = http_build_query([
            'client_id' => $testUser['id'],
            'redirect_uri' => $testUser['uri'],
            'response_type' => 'code',
            'scope' => '',
            'client_secret' => $testUser['secret'],
        ]);

        return redirect(env('APP_URL') . '/oauth/authorize?' . $query);
    }

    /**
     * Второй шаг Авторизации. Получение access token.
     * @param Request $request
     * @return mixed
     */
    public function giveAccessToken(Request $request)
    {
        $this->validate($request, [
            'client_id' => env('APP_ENV') == "local" ? 'integer' : 'required|integer',
            'redirect_uri' => env('APP_ENV') == "local" ? 'string' : 'required|string',
            'client_secret' => env('APP_ENV') == "local" ? 'string' : 'required|string',
        ]);


        /* todo: заменить на актуальные данные пользователя при тестировании. */
        $testUser = [
            'id' => '3',
            'uri' => 'http://test.local/callback',
            'secret' => 'NeR8GDzUsnb4zFCJjHP28Hpwxl5aTAIleBjy2zZR'
        ];
        $http = new Client();
        /* Запрашиваем access_token */
        $response = $http->post(env('APP_URL') . '/oauth/token', [
            'form_params' => [
                'grant_type' => 'authorization_code',
                'client_id' => $testUser['id'],
                'redirect_uri' => $testUser['uri'],
                'client_secret' => $testUser['secret'],
                'code' => $request->code,
            ],
        ]);

        // return json_decode( (string) $response->getBody(), true ); //todo в production возвращаем ответ.

        /* С использованием токена - запрашиваем цвета. Моделируем запрос клиента. */

        $accessToken = json_decode((string)$response->getBody(), true)['access_token'];

        /* Запрос к цветам */
        $response = $http->post(env('APP_URL') . '/api/colors', [
            'form_params' => [

            ],
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $accessToken,
            ],
        ]);
        return $response;
    }

    /**
     * Авторизация через приложение. С использованием логина и пароля
     * @param Request $request
     * @return mixed
     */
    public function appAuth(Request $request)
    {
        $this->validate($request, [
            'client_id' => env('APP_ENV') == "local" ? 'integer' : 'required|integer',
            'client_secret' => env('APP_ENV') == "local" ? 'string' : 'required|string',
            'username' => env('APP_ENV') == "local" ? 'email' : 'required|email',
            'password' => env('APP_ENV') == "local" ? 'string' : 'required|string',
        ]);
        $http = new Client();

        /* Получаем access_token */
        $response = $http->post(env('APP_URL') . '/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => $request->client_id,
                'client_secret' => $request->client_secret,
                'username' => $request->username,
                'password' => $request->password,
                'scope' => ''
            ],
        ]);

        //return json_decode( (string) $response->getBody(), true ); //todo в production возвращаем ответ.

        /* С использованием токена - запрашиваем цвета. Моделируем запрос клиента. */
        $accessToken = json_decode((string)$response->getBody(), true)['access_token'];

        /* Запрашиваем цвета */
        $response = $http->post(env('APP_URL') . '/api/colors', [
            'form_params' => [

            ],
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $accessToken,
            ],
        ]);
        return $response;
    }
}
