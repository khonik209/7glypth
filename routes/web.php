<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get( '/', function () {
	return view( 'welcome' );
} );

Auth::routes();

Route::get( '/home', 'HomeController@index' )->name( 'home' );

/* Спрашиваем, давать ли доступ */
Route::get( '/connect', 'Api\ApiController@authConnect' );
/* Получаем код сессии и токен */
Route::get( '/callback', 'Api\ApiController@giveAccessToken' );
