<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Install



1. Clone this
2. composer install
3. php artisan key:generate
4. php artisan migrate
5. php artisan db:seed --class=ColorsTableSeeder
6. open .env
    - check your database settings
    - check your APP_URL parameter
    - check your APP_ENV parameter (local or not)
7. npm install

Login: test@7gltphs.com

Password: Test123

## Usage

2 ways of authorization:

From web.
 
1. Make sure you are logged in. On page {http://app.local}/ you can find your client ID and secret key;
2. Redirect your user on page {http://app.local}/connect with client ID, client secret and callback URI
3. Get your access token from response. (or colors)

From API

1. Send a request on /api/connect with client_id, client_secret, email and password
2. Get your access token from response (or colors)
