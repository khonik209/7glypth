<?php

use Illuminate\Database\Seeder;

class ColorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $colors = array(
            array('name' => 'White', 'hex' => '#ffffff', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
            array('name' => 'Absolute Zero', 'hex' => '#0048BA', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
            array('name' => 'Acajou', 'hex' => '#4C2F27', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
            array('name' => 'Acid green', 'hex' => '#B0BF1A', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
            array('name' => 'Aero', 'hex' => '#7CB9E8', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
            array('name' => 'Aero blue', 'hex' => '#C9FFE5', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
            array('name' => 'African violet', 'hex' => '#B284BE', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
            array('name' => 'Air Force blue (RAF)', 'hex' => '#5D8AA8', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
            array('name' => 'Air Force blue (USAF)', 'hex' => '#00308F', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
            array('name' => 'Air superiority blue', 'hex' => '#72A0C1', 'created_at' => \Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
        );

        \Illuminate\Support\Facades\DB::table('colors')->insert($colors);

        $user = [
            [
                'email'=>'test@7glyphs.com',
                'name'=>'tester',
                'password'=>bcrypt('Test123')
            ]
        ];
        \Illuminate\Support\Facades\DB::table('users')->insert($user);
    }
}
