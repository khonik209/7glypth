/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

import Clients from './components/passport/Clients';
Vue.component('passport-clients', Clients);

import AuthorizedClients from './components/passport/AuthorizedClients';
Vue.component('passport-authorized-clients', AuthorizedClients);

import AccessTokens from './components/passport/PersonalAccessTokens';
Vue.component('passport-personal-access-tokens', AccessTokens);

var app = new Vue({
    el: '#app'
});
